

**dari alvin** : Ngebuat billing warnet

Judul aplikasi proyek akhir

Timer otomatis rental video game

Deskripsi proyek
Timer yang menjaga waktu dan uang yang dibutuhkan untuk rental video game

Fungsi dan manfaat karya
Membantu pemilik rental menjaga waktu secara otomatis dan pemain mengetahui waktu dan uang yang harus dibayarkan
Perkiraan tipe avr yang digunakan
Atmega8515


Perangkat masukan dan keluaran yang digunakan
In : button
Out : led , lcd


Fitur dan cara kerja

1.Ketika memulai akan ditekan tombol start , lalu timer akan berjalan mundur sesuai waktu dan harga yang ditentukan(timer dan harga akan ditampilkan lewat lcd)
2.Ketika sewa akan habis akan ada dua kali peringatan.Led 1 akan menyala saat waktu tinggal 5 menit terakhir dan saat 1 menit terakhir led akan nyala 2.
3.Saat waktu habis led akan menyala 3
4.Player bisa menambah waktu sewa dengan menambahkan biaya dan memberitahu operator.
5.Akan ada tombol stop untuk menghentikan waktu sementara.
6.Dan akan ada juga tombol end apabila playernya berhenti ketika sewa belum habis.



---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
dari valen :   jadi ada button buat tambah air. setelah bbrp counter kadar airnya akan menurun kalo kadar air udah dibawah nilai tertentu, dia display alarm dari led sama lcd nyuruh isi air

dari rara : Sistem untuk absensi



-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
**dari hotma** : 

Judul aplikasi proyek akhir 
Cashier Machine

Deskripsi proyek 
menghitung harga total dari produk yang dibeli dan mengeluarkan output berupa total harga

Fungsi dan manfaat karya 
Berfungsi sebagai alat penghitung harga barang

Perkiraan tipe AVR yang digunakan 
ATmega8515

Perangkat masukan dan keluaran yang akan digunakan 
Masukan : keypad, button 
Keluaran : led, lcd

Fitur dan Flow cara kerja sistem (aplikasi) yang dibuat 
1. User akan mulai memasukan kode barang yang telah diberikan 
2. Apabila mesin tidak menerima input dalam 5 detik, akan langsung menghitung totalnya 
3. Led akan menjadi indikator jumlah jenis barang yang telah diinput 
4. Apabila mesin telah menerima input sebanyak maksimal led yg diberikan, akan langsung menghitung totalnya 
5. Sebagian barang ada yang didiskon, sehingga nantinya program akan menghitung hasil diskon terlebih dahulu baru melanjutkan perhitungan 
6. menekan tombol submit untuk langsung menghitung totalnya, tanpa memperdulikan timer ataupun jumlah barang

Mesin menggunakan RAM sebagai tempat penyimpanan data barang
